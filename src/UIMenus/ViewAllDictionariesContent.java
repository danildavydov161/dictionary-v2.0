package UIMenus;

import Interfaces.IDictionary;
import Resources.ConsoleUIResources;

import java.util.List;

public class ViewAllDictionariesContent {

    List<IDictionary> dictionaries;
    ConsoleUIResources consoleUIResources;

    public ViewAllDictionariesContent(List<IDictionary> dictionaries, ConsoleUIResources consoleUIResources) {
        this.dictionaries = dictionaries;
        this.consoleUIResources = consoleUIResources;
    }

    public void viewAllContent(){
        try{
            for (int i = 0; i < dictionaries.size(); i++){
                System.out.println(dictionaries.get(i).read());
            }
        }
        catch (Exception e){
            System.out.println("Словарей нет");
        }
    }

}
