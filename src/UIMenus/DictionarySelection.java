package UIMenus;

import Interfaces.IDictionary;
import Resources.ConsoleUIResources;

import java.util.List;

public class DictionarySelection {

    List<IDictionary> dictionaries;
    ConsoleUIResources consoleUIResources;

    public DictionarySelection(List<IDictionary> dictionaries, ConsoleUIResources consoleUIResources) {
        this.dictionaries = dictionaries;
        this.consoleUIResources = consoleUIResources;
    }

    public void dictionarySelection() throws Exception {

        for (int i = 0; i < dictionaries.size(); i++) {
            System.out.println(i + 1 + ". " + dictionaries.get(i).toString().substring(dictionaries.get(i).toString().indexOf('.')
                    + 1, dictionaries.get(i).toString().indexOf('@')));
        }

        consoleUIResources.setDictionaryIndex();

        while (consoleUIResources.getActiveDictionaryMenu()) {
            System.out.println("Выберите действие:\n1. Просмотр содержимого\n" +
                    "2. Просмотр элемента по ключу\n3. Добавить элемент\n4. Удалить элемент\n5. Вернуться назад\n6. Выход");
            consoleUIResources.setMove();
            switch (consoleUIResources.getMove()) {
                case "1":
                    try {
                        System.out.println(dictionaries.get(Integer.parseInt(consoleUIResources.getDictionaryIndex()) - 1).read());
                    } catch (RuntimeException e) {
                        System.out.println("Словарь пуст");
                    }
                    break;
                case "2":
                    System.out.println("Введите ключ:");
                    consoleUIResources.setKey();

                    try {
                        if (dictionaries.get(Integer.parseInt(consoleUIResources.getDictionaryIndex()) - 1).readByKey(consoleUIResources.getKey()) == null)
                            System.out.println("Такого элемента не существует");
                        else
                            System.out.println(dictionaries.get(Integer.parseInt(consoleUIResources.getDictionaryIndex()) - 1).readByKey(consoleUIResources.getKey()));
                    } catch (RuntimeException e) {
                        System.out.println("Такого элемента не существует");
                    }
                    break;
                case "3":
                    System.out.println("Введите ключ:");
                    consoleUIResources.setKey();
                    System.out.println("Введите значение:");
                    consoleUIResources.setValue();
                    dictionaries.get(Integer.parseInt(consoleUIResources.getDictionaryIndex()) - 1).write(consoleUIResources.getKey(), consoleUIResources.getValue());
                    break;
                case "4":
                    System.out.println("Введите ключ:");
                    consoleUIResources.setKey();
                    try {
                        if (dictionaries.get(Integer.parseInt(consoleUIResources.getDictionaryIndex()) - 1).readByKey(consoleUIResources.getKey()) == null)
                            System.out.println("Такого элемента не существует");
                        else {
                            System.out.printf("Элемент %s удален из словаря\n", dictionaries.get(Integer.parseInt(consoleUIResources.getDictionaryIndex()) - 1).readByKey(consoleUIResources.getKey()));
                            dictionaries.get(Integer.parseInt(consoleUIResources.getDictionaryIndex()) - 1).delete(consoleUIResources.getKey());
                        }
                    } catch (RuntimeException e) {
                        System.out.println("Такого элемента не существует");
                    }
                    break;
                case "5":
                    consoleUIResources.setActiveDictionaryMenu(false);
                    break;
                case "6":
                    consoleUIResources.setActiveMainMenu(false);
                    consoleUIResources.setActiveDictionaryMenu(false);
                    break;
            }
        }
    }
}
