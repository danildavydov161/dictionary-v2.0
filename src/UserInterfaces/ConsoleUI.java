package UserInterfaces;

import Entitys.EntitysCreator;
import Interfaces.IDictionary;
import Resources.*;
import UIMenus.*;

import java.util.List;

public class ConsoleUI {
    private DictionariesCreator dIctionariesCreator;
    private List<IDictionary> dictionaries;
    private ConsoleUIResources consoleUIResources;
    private DictionarySelection dictionarySelection;
    private ViewAllDictionariesContent viewAllDictionariesContent;

    public ConsoleUI() throws Exception {
        dIctionariesCreator = EntitysCreator.getDictionariesCreator();
        dictionaries = dIctionariesCreator.getDictionaries();
        consoleUIResources = EntitysCreator.getConsoleUIResources();
        dictionarySelection = EntitysCreator.getDictionarySelection();
        viewAllDictionariesContent = EntitysCreator.getViewAllDictionariesContent();

    }

    public void start() throws Exception {
        while(consoleUIResources.getActiveMainMenu()){
            consoleUIResources.setActiveDictionaryMenu(true);
            System.out.println("Выберите действие:\n1. Выбор словаря\n2. Просмотр содержимого всех словарей\n3. Выход");
            consoleUIResources.setMove();
            switch (consoleUIResources.getMove()){
                case "1":
                    dictionarySelection.dictionarySelection();
                    break;
                case "2":
                    viewAllDictionariesContent.viewAllContent();
                    break;
                case "3":
                    consoleUIResources.setActiveMainMenu(false);
                    break;
                default:
                    System.out.println("Неверный ввод");
            }
        }
    }

}
