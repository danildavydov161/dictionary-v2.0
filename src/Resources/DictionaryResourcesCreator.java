package Resources;

import DAO.WordDao;
import Enums.DictionaryType;
import Services.WordService;

public class DictionaryResourcesCreator {
    private WordService wordService;
    private WordDao wordDao;

    public DictionaryResourcesCreator(DictionaryType dictionaryType){
        wordService = new WordService(dictionaryType);
        wordDao = wordService.getWordDao();
    }

    public WordDao getWordDao(){
        return wordDao;
    }

    public WordService getWordService(){
        return wordService;
    }

}
