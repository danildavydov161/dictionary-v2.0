package Resources;

import Dictionaries.*;
import Interfaces.IDictionary;

import java.util.ArrayList;
import java.util.List;

public class DictionariesCreator {

    List<IDictionary> dictionaries;

    public DictionariesCreator() throws Exception {
        dictionaries = new ArrayList<>();
        dictionaries.add(new LetterDictionary());
        dictionaries.add(new NumberDictionary());
    }

    public List<IDictionary> getDictionaries(){
        return dictionaries;
    }
}
