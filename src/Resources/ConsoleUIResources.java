package Resources;

import java.util.Scanner;

public class ConsoleUIResources {

    private Scanner scanner;
    private String move;
    private String key;
    private String value;
    private String dictionaryIndex;
    private boolean activeMainMenu;
    private boolean activeDictionaryMenu;

    public ConsoleUIResources(){
        scanner = new Scanner(System.in);
        activeMainMenu = true;
    }

    public String getMove(){
        return move;
    }

    public String getKey(){
        return key;
    }

    public String getValue(){
        return value;
    }

    public String getDictionaryIndex(){
        return dictionaryIndex;
    }

    public boolean getActiveMainMenu(){
        return activeMainMenu;
    }

    public boolean getActiveDictionaryMenu(){
        return activeDictionaryMenu;
    }

    public void setMove(){
        move = scanner.nextLine();
    }

    public void setKey(){
        key = scanner.nextLine();
    }

    public void setValue(){
        value = scanner.nextLine();
    }

    public void setDictionaryIndex(){
        dictionaryIndex = scanner.nextLine();
    }

    public void setActiveMainMenu(boolean activeMainMenu){
        this.activeMainMenu = activeMainMenu;
    }

    public void setActiveDictionaryMenu(boolean activeDictionaryMenu){
        this.activeDictionaryMenu = activeDictionaryMenu;
    }
}
