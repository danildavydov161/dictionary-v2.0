package Validators;

import Interfaces.IValidator;

import java.util.regex.Pattern;

public class LetterValidator implements IValidator {
    @Override
    public boolean validate(String word) {

        return Pattern.matches("^[A-Za-z]{4}$", word);

    }
}
