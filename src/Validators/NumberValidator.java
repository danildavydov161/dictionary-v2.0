package Validators;

import Interfaces.IValidator;

import java.util.regex.Pattern;

public class NumberValidator implements IValidator {
    @Override
    public boolean validate(String word) {

        return Pattern.matches("^[0-9]{5}$", word);

    }
}
