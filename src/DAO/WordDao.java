package DAO;

import Entitys.Word;
import Interfaces.IWordDao;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class WordDao implements IWordDao {

    private List<Word> wordList = new ArrayList<>();

    private String fileName;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void update() {


        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            if (getAll().size() != 0) {
                for (int i = 0; i < getAll().size(); i++) {
                    bw.append(getAll().get(i).getKey() + " - " + getAll().get(i).getValue() + "\n");
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public List<Word> read() throws IOException {

        List<Word> list = new ArrayList<>();

        if (Files.exists(Paths.get(fileName))) {
            BufferedReader br = new BufferedReader(new FileReader(fileName));

            br.lines().forEach(el -> {
                String[] strings = el.split(" - ", 2);
                list.add(new Word(strings[0], strings[1]));
            });
        }

        setDictionary(list);

        return list;
    }

    @Override
    public void addWord(String key, String value) {

        Word word = new Word(key, value);
        {
            if (isWordMissing(key, value))
                wordList.add(word);
        }
    }

    private boolean isWordMissing(String key, String value) {

        if (wordList.stream().filter(w -> w.getKey().contentEquals(key)).findFirst().orElse(null) == null &&
                wordList.stream().filter(w -> w.getValue().contentEquals(value)).findFirst().orElse(null) == null)
            return true;

        return false;
    }

    @Override
    public Word readWord(String key) {

        try {
            for (Word word : wordList) {
                if (word.getKey().equals(key)) {
                    return word;
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return null;

    }

    @Override
    public void deleteWord(String key) {

        for (Word word : wordList) {
            if (word.getKey().contentEquals(key)) {
                wordList.remove(word);
                break;
            }
        }

    }

    @Override
    public List<Word> getAll() {
        return wordList;
    }

    @Override
    public void setDictionary(List<Word> words) {
        wordList.removeAll(wordList);
        wordList = words;
    }

}
