package Dictionaries;

import DAO.WordDao;
import Entitys.Word;
import Services.WordService;
import Enums.DictionaryType;
import Interfaces.IDictionary;
import Resources.*;

import java.io.IOException;
import java.util.List;

public class NumberDictionary implements IDictionary {
    private DictionaryType dictionaryType = DictionaryType.NUMBER_DICTIONARY;
    private DictionaryResourcesCreator dictionaryResourcesCreator = new DictionaryResourcesCreator(dictionaryType);
    private WordService wordService = dictionaryResourcesCreator.getWordService();
    private WordDao wordDao = dictionaryResourcesCreator.getWordDao();
    private String fileName;

    public NumberDictionary() throws Exception {
        fileName = dictionaryType.toString() + ".txt";
        wordDao.setFileName(fileName);

        read();
    }

    public void write(String key, String value) throws Exception {

        wordService.write(key, value);

        wordDao.update();

    }

    public List<Word> read() throws IOException {

        return wordDao.read();

    }

    public Word readByKey(String key) {

        return wordDao.readWord(key);
    }

    public void delete(String key) {
        wordDao.deleteWord(key);
        wordDao.update();
    }


}
