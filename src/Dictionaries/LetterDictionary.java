package Dictionaries;

import DAO.WordDao;
import Entitys.Word;
import Services.WordService;
import Enums.DictionaryType;
import Interfaces.IDictionary;
import Resources.*;

import java.io.IOException;
import java.util.List;

public class LetterDictionary implements IDictionary {

    private DictionaryType dictionaryType;
    private DictionaryResourcesCreator dictionaryResourcesCreator;
    private WordService wordService;
    private WordDao wordDao;
    private String fileName;

    public LetterDictionary() throws Exception {
        dictionaryType = DictionaryType.LETTER_DICTIONARY;
        dictionaryResourcesCreator = new DictionaryResourcesCreator(dictionaryType);
        wordService = dictionaryResourcesCreator.getWordService();
        wordDao = dictionaryResourcesCreator.getWordDao();
        fileName = dictionaryType.toString() + ".txt";
        wordDao.setFileName(fileName);

        wordDao.read();
    }

    public void write(String key, String value) throws Exception {

        wordService.write(key, value);

        wordDao.update();

    }

    public List<Word> read() throws IOException {

        return wordDao.read();

    }

    public Word readByKey(String key) {

        return wordService.getWordDao().readWord(key);
    }

    public void delete(String key) {
        wordService.getWordDao().deleteWord(key);
        wordDao.update();
    }


    public DictionaryType getDictionaryType() {
        return dictionaryType;
    }

}
