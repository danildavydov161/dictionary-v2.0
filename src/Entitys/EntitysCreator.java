package Entitys;

import Interfaces.IDictionary;
import Resources.ConsoleUIResources;
import Resources.DictionariesCreator;
import UIMenus.DictionarySelection;
import UIMenus.ViewAllDictionariesContent;

import java.util.List;

public abstract class EntitysCreator {
    private static DictionariesCreator dictionariesCreator;
    private static List<IDictionary> dictionaries;
    private static ConsoleUIResources consoleUIResources;
    private static DictionarySelection dictionarySelection;
    private static ViewAllDictionariesContent viewAllDictionariesContent;

    static {
        try {
            dictionariesCreator = new DictionariesCreator();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        dictionaries = dictionariesCreator.getDictionaries();
        consoleUIResources = new ConsoleUIResources();
        dictionarySelection = new DictionarySelection(dictionaries, consoleUIResources);
        viewAllDictionariesContent = new ViewAllDictionariesContent(dictionaries, consoleUIResources);

    }

    public static DictionariesCreator getDictionariesCreator(){
        return dictionariesCreator;
    }

    public static List<IDictionary> getDictionaries(){
        return dictionaries;
    }

    public static ConsoleUIResources getConsoleUIResources(){
        return consoleUIResources;
    }

    public static DictionarySelection getDictionarySelection(){
        return dictionarySelection;
    }

    public static ViewAllDictionariesContent getViewAllDictionariesContent(){
        return viewAllDictionariesContent;
    }
}
