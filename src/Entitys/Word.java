package Entitys;

public class Word {

    private String key;
    private String value;

    public Word(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getKey(){
        return key;
    }


    public String toString() {
        return key + " - " + value;
    }
}
