package Interfaces;

import Enums.DictionaryType;

public interface IValidator {
    boolean validate(String word);
}
