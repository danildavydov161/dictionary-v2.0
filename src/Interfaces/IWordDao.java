package Interfaces;

import Entitys.Word;

import java.util.*;

public interface IWordDao {
    void addWord(String key, String value);
    Word readWord(String key);
    void deleteWord(String key);
    List<Word> getAll();

    void setDictionary(List<Word> words);
}
