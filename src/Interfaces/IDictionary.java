package Interfaces;

import Entitys.Word;
import Enums.DictionaryType;

import java.io.IOException;
import java.util.List;

public interface IDictionary {

    void write(String key, String value) throws Exception;
    Word readByKey(String key);
    void delete(String key);
    List<Word> read() throws IOException;
}
