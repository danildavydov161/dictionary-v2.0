package Interfaces;

import DAO.WordDao;

public interface IWordService {
    void write(String key, String value);
    WordDao getWordDao();
}
