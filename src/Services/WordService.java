package Services;

import DAO.WordDao;
import Enums.DictionaryType;
import Interfaces.IValidator;
import Interfaces.IWordService;
import Validators.*;

public class WordService implements IWordService {
    private IValidator validator;
    private WordDao wordDao;

    public WordService(DictionaryType dictionaryType) {

        wordDao = new WordDao();
        if (dictionaryType == DictionaryType.LETTER_DICTIONARY)
            validator = new LetterValidator();
        else if (dictionaryType == DictionaryType.NUMBER_DICTIONARY)
            validator = new NumberValidator();

    }

    @Override
    public void write(String key, String value) {

        if (validator.validate(key) && validator.validate(value))
            wordDao.addWord(key, value);

    }

    @Override
    public WordDao getWordDao() {
        return wordDao;
    }


}
